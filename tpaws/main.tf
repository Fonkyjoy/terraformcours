resource "aws_instance" "webserver_1"{
  ami = "ami-00f53c9391166b018" # l'image de mon instance
  instance_type = "t2.micro"
}
output "var" {
    value = var.token
    sensitive = true
}

// remplace interface graphique pour monter le secret
/* resource "vault_mount" "kv_aws" {
    path = "kv_aws_t" 
    type = "kv-v2"
    description = "Exemple key value"
} */

// recupération des données dans le vault clé fait en interface graphique
data "vault_generic_secret" "damien_secret_kp" {
   //chemin dans le vault
    path = "kv_aws/aws_ssh"
}

// output pour info log
output "var2"{
    value = data.vault_generic_secret.damien_secret_kp.data["public_key_aws"]
    sensitive = true
}

// path sur vault de la donnée de la clé fait en interface graphique
resource "aws_key_pair" "damien_pair" {
  // indication de la cle
  key_name = "damien-keypair_kp"
  public_key = data.vault_generic_secret.damien_secret_kp.data["public_key_aws"]
}


