provider "vault" {
  address = "http://135.125.13.91:8080"
  token = var.token
}

# credentials
provider "aws" {
  region                   = "us-east-2" // indication de la zone Ohio
  shared_config_files      = ["C:/Users/dfrage/.aws/config"]
  shared_credentials_files = ["C:/Users/dfrage/.aws/credentials"]
  profile                  = "default"
}
