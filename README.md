
***Principale:***  
Init ==> **terraform init**  
Plan ==> **terraform plan**  
Validate ==> **terraform validate**   
Apply  ==> **terraform apply –auto-approve**  
Destroy ==> terraform destroy   
  
***Autre:***  
Met à jour le state par rapport au provider ==> **terraform   refresh**   
Visualisation du state courant ==> **terraform show**   

Importer des commandes traquées non présent dans le terraform state ==> **terraform import**  
Retirrer un attachement ==> terraform state rm nom_de_la_ressource  

Visualisation des intitulés des ressources appliquées ==> **terraform state list**  
visualiser un objet specifique ==> **terraform state show nom_de_le_ressource**  
*exemple: terraform state show aws_iam_user.user1*  

liste les valeurs de sortie ==> **terraform output**  

# Stucture  
- *provider* : comprendre les informations de credentials  
- *exemple* : creation de bucket avec association de key pair  
- *exo*  : 
    - *exo1*: creation de groupe et de users  
    - *infra1*: creation d'un environnement sous aws, avec ec2, dns, elastic ip, sous réseau,...  
    - *remote-state-demo*: demonstartion de recupération de state pour travail en equipe  
    - *tpaws*: creation d'une connexion ec2 avec ssh, cle stocké sous vault  

# Variable  

# Lien

