# 1 VPC
resource "aws_vpc" "infra1" {
	cidr_block = "10.0.0.0/16"
	enable_dns_hostnames = true // activation du support dns

	tags = {
		Name = var.tag_name
	}
}

# 2 Internet Gateway
resource "aws_internet_gateway" "infra1" {
	vpc_id = aws_vpc.infra1.id //id de la passerelle internet 

	tags = {
		Name = var.tag_name
	}
}

# 3 Tables de Routage
resource "aws_route_table" "infra1" {
	vpc_id = aws_vpc.infra1.id
	route {
    cidr_block = "0.0.0.0/0" //IPv4 autorise a sortir vers
    //toute les @ ips
    gateway_id = aws_internet_gateway.infra1.id
  }

  tags = {
		Name = var.tag_name
	}
}

# 4 Sous-Réseau Public
# Sous-réseau 1
resource "aws_subnet" "infra1_subnet1" {
  vpc_id = aws_vpc.infra1.id
  cidr_block = "10.0.1.0/24" // découpage du sous-réseau
  availability_zone = "${var.region}a" //templating syntaxe

  tags = {
		Name = "${var.tag_name} - subnet 1"
	}
}

# 4 Sous-Réseau Public
# Sous-réseau 2 pour zone de disponiblité différente
resource "aws_subnet" "infra1_subnet2" {
  vpc_id = aws_vpc.infra1.id
  cidr_block = "10.0.2.0/24" // découpage du sous-réseau
  availability_zone = "${var.region}b" //templating syntaxe

  tags = {
		Name = "${var.tag_name} - subnet 2"
	}
}

# 5 Association sous-réseau / table de routage
resource "aws_route_table_association" "infra1_subnet1" {
  subnet_id      = aws_subnet.infra1_subnet1.id
  route_table_id = aws_route_table.infra1.id
// association entre une table et un sous-réseau
}

# 5 Association sous-réseau / table de routage
resource "aws_route_table_association" "infra1_subnet2" {
  subnet_id      = aws_subnet.infra1_subnet2.id
  route_table_id = aws_route_table.infra1.id
// association entre une table et un sous-réseau
}

# 6 Groupe de sécurité(règle firewall) 
# autorisation des points d'entrées pour SSH et HTTP
resource "aws_security_group" "infra1"{
  name        = "allow_web"
  description = "Allow HTTP and SSH"
  vpc_id      = aws_vpc.infra1.id

  ingress {
    description      = "HTTP frow anywhere"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "SSH frow anywhere"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

    ingress {
    description      = "Node frow anywhere"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  // ouverture complète en sortie Port/ Ip / Protocole
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1" // tous les protocoles
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = var.tag_name
  }
}

# 7 Interface de réseau elastic(ENI)
resource "aws_network_interface" "infra1" {
  subnet_id       = aws_subnet.infra1_subnet1.id
  private_ips     = [var.private_ip] // liste d' @ ip 
  security_groups = [aws_security_group.infra1.id] // liste de groupe
  
  tags = {
    Name = var.tag_name
  }
}

//second interface pour tp1
resource "aws_network_interface" "infra1b" {
  subnet_id       = aws_subnet.infra1_subnet1.id
  private_ips     = [var.private_ip2] // liste d' @ ip 
  security_groups = [aws_security_group.infra1.id] // liste de groupe
  
  tags = {
    Name = var.tag_name2
  }
}

# 8 Ip Elastic
resource "aws_eip" "infra1" {
  network_interface         = aws_network_interface.infra1.id
  associate_with_private_ip = var.private_ip
  depends_on                = [aws_internet_gateway.infra1] // impose une ressource avant l'utilisation
  vpc                       = true
  tags = {
    Name = var.tag_name
  }
}

# 8 Ip Elastic de la seconde machine
resource "aws_eip" "infra1b" {
  network_interface         = aws_network_interface.infra1b.id
  associate_with_private_ip = var.private_ip2
  depends_on                = [aws_internet_gateway.infra1] // impose une ressource avant l'utilisation
  vpc                       = true
  tags = {
    Name = var.tag_name2
  }
}


# 9 EC Server Web
resource "aws_instance" "infra1"{
  ami           = var.ami_id //Ubuntu Server 20.04 Ohio
  instance_type = "t2.micro"
  key_name = "infra1_damien"

  network_interface {
  device_index = 0 
  network_interface_id = aws_network_interface.infra1.id
  }

// script à executer au lancement initial de la vm
  user_data = file("install_apach.sh")

  tags = {
    Name = var.tag_name
  }
}

// creation de l'intance tp1
resource "aws_instance" "infra1b"{
  ami           = var.ami_id //Ubuntu Server 20.04 Ohio
  instance_type = "t2.micro"
  key_name      = aws_key_pair.deployer.key_name // liaison de la clé

  network_interface {
  device_index = 0 
  network_interface_id = aws_network_interface.infra1b.id
  }

  tags = {
    Name = var.tag_name2
  }
}

// resource key pair public local
resource "aws_key_pair" "deployer" {
  key_name   = "public_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQClYwjmSXE7tSzzJZPNjaXDH5L/AC6KumNJyTJTrZxlfy88sLl1CPFjzaxIRAyJ37ZsJXa9/yBlVcrGvprEwcl8DZZRpAutnEe7AxD67d4eITjXHqCwP25kUT2yNMa6cZz+Y+baYarpvv6X+yUM+54UsUxHf3Uk2qCGPTwl/cb5ZtL8ANJOJdAbVqgpau5ZsGMa+vA4h/zSxFIVVcwCRI1BgOAbcKsV3ShTdIksnH9jUIn7kmDSLhp6RSzitNyR6QauTXClE0k4BdfnO2fBMif9aiePCOJrZyUTt4kzMCgozUbSBgle1BgEoLVFQXbtAZeTric6rgkV0NQPIpN5T3WfaDetC9oruhOiS+gkuae5siGEzcbT2uzZvw4mohzeCT2mEY2T5LOd/68piMyzeB2wP+vPDloPAwc0q1verU39b96Z0bRQ7oRGHMeKv/+b/20qkLsqoXCkhH1QNjD3+z2YTlwnAAgM3pcW4nSUobctOtnMemWgEoq8mSb9WFnC03s= dfrage@J1364619"
}

# 10 DB server (instanceRDS)