# credentials
provider "aws" {
  region                   = "us-east-2" // indication de la zone Ohio
  shared_config_files      = ["C:/Users/dfrage/.aws/config"]
  shared_credentials_files = ["C:/Users/dfrage/.aws/credentials"]
  profile                  = "default"
}
