*** TP 1 ***
1.a
Modifier la configuration de l'infrastructure "infra1" comme suit:
- création d'une keypair. La clé publique qu'elle embarquera devra être générée à votre niveau. 
Vous disposerez donc également de la clé privée correspondant.
- création d'une instance ec2 de type t2.micro/ubuntu 22.04 (si dispo dans votre région) utilisant cette keypair

1.b
Vérifier que vous pouvez vous connecter en ssh à cette instance ec2.
Manuellement: 
- installer nodejs et npm dans l'instance (sudo apt install nodejs npm)
- git cloner ce dépôt: https://github.com/cdufour/simpleweb.git
- exécuter les commandes "npm install" puis "node server.js" pour démarrer le serveur web
- vérifier que vous avez accès à l'application par requête http sur l'instance ec2

