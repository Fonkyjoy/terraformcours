# declaration des variables
variable "keyname" {
  type = string
  default =  "keyp-damien" // valeur par defaut
}

variable "musketeers"{
  type = list(string)
}

// creation d'une ec2
# resource "aws_s3_bucket" "b" {
#   # nom du bucker
#   bucket = "formation-terraform-luminess-damien-frage-2"
#   tags = {
#     Name        = "My bucket"
#     Environment = "Dev"

#   }
# }

// creation d'une key pair
resource "aws_key_pair" "keyp" {
  key_name   = var.keyname // appel à la variable
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQClYwjmSXE7tSzzJZPNjaXDH5L/AC6KumNJyTJTrZxlfy88sLl1CPFjzaxIRAyJ37ZsJXa9/yBlVcrGvprEwcl8DZZRpAutnEe7AxD67d4eITjXHqCwP25kUT2yNMa6cZz+Y+baYarpvv6X+yUM+54UsUxHf3Uk2qCGPTwl/cb5ZtL8ANJOJdAbVqgpau5ZsGMa+vA4h/zSxFIVVcwCRI1BgOAbcKsV3ShTdIksnH9jUIn7kmDSLhp6RSzitNyR6QauTXClE0k4BdfnO2fBMif9aiePCOJrZyUTt4kzMCgozUbSBgle1BgEoLVFQXbtAZeTric6rgkV0NQPIpN5T3WfaDetC9oruhOiS+gkuae5siGEzcbT2uzZvw4mohzeCT2mEY2T5LOd/68piMyzeB2wP+vPDloPAwc0q1verU39b96Z0bRQ7oRGHMeKv/+b/20qkLsqoXCkhH1QNjD3+z2YTlwnAAgM3pcW4nSUobctOtnMemWgEoq8mSb9WFnC03s= dfrage@J1364619"
}

//creation d'une instance 
resource "aws_instance" "web" {
  ami           = "ami-0283a57753b18025b" //ubuntu 22.04
  instance_type = "t3.micro"
  key_name      = aws_key_pair.keyp.key_name // liaison de la clé
  tags = {
    Name = "HelloWorld_damien"
    keyname = var.keyname // appel à la variable
    Musketeers = var.musketeers[0]
  }
}