terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.45.0"
    }
  }
}

# credentials
provider "aws" {
  region                   = "us-east-2" // indication de la zone
  shared_config_files      = ["C:/Users/dfrage/.aws/config"]
  shared_credentials_files = ["C:/Users/dfrage/.aws/credentials"]
  profile                  = "default"
}

