*** exo 1 ***
Note: chez AWS, le service IAM (Identity and Access Management) est le service qui permet de gérer les groupes,
les utilisateurs et les "json policies" (politiques d'accès)

Ecrice un fichier exo1.tf qui permettra de créer:
- un groupe nommé "group-<votre-nom-prenom>"
- deux utilisateurs nommés "user-<votre-nom-prenom>-1" et "user-<votre-nom-prenom>-2"
Ces utilisateurs devront être associés au groupe créé.