
// creation  groupe groupe-frage-damien
resource "aws_iam_group" "groupe-frage-damien" {
  name = "groupe-frage-damien"
  path = "/users/" // optional
}
// import ressource aws dont ont-est pas l'auteur
# resource "aws_iam_group" "gp-marco" {
#   name = "marco"
# }

// creation user 1
resource "aws_iam_user" "user1" {
  name = "frage-damien-1"
}
// creation user 2
resource "aws_iam_user" "user2" {
  name = "frage-damien-2"
}

// rattachement des users a un groupe
resource "aws_iam_group_membership" "team" {
  name = "tf-testing-group-membership"
  users = [
    aws_iam_user.user1.name,
    aws_iam_user.user2.name,
  ]
  group = aws_iam_group.groupe-frage-damien.name
}




